<?php

namespace App\Pdf;

class CV extends \TCPDF {

    public function __construct() {
        parent::__construct();

        $this->SetMargins(self::MARGIN_LEFT, self::MARGIN_TOP, self::MARGIN_RIGHT);
        $this->SetAutoPageBreak(true, self::MARGIN_BOTTOM);

        // add fonts
        //opensansb-opensansbi-opensansextrab-opensansextrabi-opensansi-opensanslight-opensanslighti-opensans-opensanssemib-opensanssemibi-
        foreach (glob('./fonts/*.ttf') as $fontPath) {
            \TCPDF_FONTS::addTTFfont($fontPath, 'TrueTypeUnicode','', 32);
        }
    }

    // MARGIN
    CONST MARGIN_TOP = 55;
    CONST MARGIN_LEFT = 10;
    CONST MARGIN_RIGHT = 10;
    CONST MARGIN_BOTTOM = 20;

    // #209cee
    CONST BLUE_COLOR = array(32, 156, 238);
    // #363636
    CONST BLACK_COLOR = array(54, 54, 54);

    // HELPERS
    CONST TITLE_MARGIN_BOTTOM = 5;
    CONST PARAGRAPH_MARGIN_BOTTOM = 4;

    // EXPERIENCE ELEMENTS
    CONST CIRCLE_RADIUS = 3;
    CONST CIRCLE_X = 15;
    CONST DATE_X = 25;
    CONST DESCRIPTION_X = 55;
    CONST LINE_WIDTH = 1;

    // SKILL
    CONST SKILL_WIDTH = 60;
    CONST SKILL_CIRCLE_RADIUS = 2;
    CONST SKILL_CIRCLE_LINE_WIDTH = 0.75;

    // TRAINING
    CONST TRAINING_WIDTH = 60;

    CONST BORDER = 0;

    CONST FONT_NAME = 'opensans';
    CONST FONT_NAME_SEMI = 'opensanssemib';
    CONST FONT_NAME_LIGHT = 'opensanslight';

    private $name = '';
    private $email = '';
    private $position = '';
    private $phone = '';
    private $age = '';
    private $vehicule = '';
    private $nationality = '';
    private $address = '';
    private $zip = '';
    private $city = '';
    private $country = '';

    public function Header() {
        $this->Rect(0, 0, $this->getPageWidth(), self::MARGIN_TOP - 5, 'F', null, self::BLUE_COLOR);
        
        $textX = self::DESCRIPTION_X;
        $topY = 28;

        $avatarPath = 'images/avatar.jpg';
        $this->Image($avatarPath, 10, 7, 37, 0);

        $this->SetTextColor(255,255,255);
        $this->SetXY($textX, 5);
        $this->SetFont(self::FONT_NAME, 'B', 24);
        $this->SetX($textX);
        $this->Cell(0, 0, $this->name, 0, 1);
        $this->SetFont(self::FONT_NAME_SEMI, '', 16);
        $this->SetX($textX);
		$this->Cell(0, 0, $this->position, 0, 1);
    
        $this->SetY($topY);
        $this->SetFont(self::FONT_NAME, '', 12);
        $this->SetX($textX);
        $this->Cell(0, 0, "{$this->address} {$this->zip} {$this->city}", 0, 1);
        $this->SetX($textX);
        $this->Cell(0, 0, $this->email, 0, 1);
        $this->SetX($textX);
        $this->Cell(0, 0, $this->phone, 0, 1);
        
        $this->SetY($topY);
		$this->Cell(0, 0, $this->age, 0, 1, 'R');
		$this->Cell(0, 0, $this->vehicule, 0, 1, 'R');
		$this->Cell(0, 0, $this->nationality, 0, 1, 'R');
    }

    public function setHeaderInfo($name, $position, $email, $phone, $age, $vehicule, $nationality, $address, $zip, $city, $country) {
        $this->name = $name;
        $this->email = $email;
        $this->position = $position;
        $this->phone = $phone;
        $this->age = $age;
        $this->vehicule = $vehicule;
        $this->nationality = $nationality;
        $this->address = $address;
        $this->zip = $zip;
        $this->city = $city;
        $this->country = $country;
    }

    public function addY($height) {
        $this->SetY($this->getY() + $height);
    }

    public function addTitle($title) {
        $this->SetFont(self::FONT_NAME, '', 14);
        $this->Cell(0, 0, $title, 0, 1);
        $this->addY(self::TITLE_MARGIN_BOTTOM);
    }

    public function addParagraph($width, $text) {
        $this->SetFont(self::FONT_NAME, '', 12);
        $border = self::BORDER;
        $this->Multicell($width, 0, $text, 0, 'L');
        $this->addY(self::PARAGRAPH_MARGIN_BOTTOM);
    }

    public function getExperienceHeight($start, $end, $position, $company, $description, $skills) {
        // CIRCLE
        $circleHeight = self::CIRCLE_RADIUS;

        // DATE FONT
        $this->SetFont(self::FONT_NAME, '', 12);

        // DATE START
        $dateHeight = $this->GetStringHeight(25, $start);

        // DATE END
        $dateHeight += $this->GetStringHeight(25, $end);

        // DESCRIPTION
        $this->SetFont(self::FONT_NAME, 'B', 12);
        $descriptionHeight = $this->GetStringHeight(65, $position);
        $this->SetFont(self::FONT_NAME, '', 12);
        $descriptionHeight += $this->GetStringHeight(80, $company);

        $this->SetFont(self::FONT_NAME, '', 12);
        $descriptionHeight += $this->GetStringHeight(145, $description);

        $this->SetFont(self::FONT_NAME_LIGHT, 'I', 12);
        $descriptionHeight += $this->GetStringHeight(145, $skills);

        $descriptionHeight += self::PARAGRAPH_MARGIN_BOTTOM;
        
        return max($circleHeight, $dateHeight, $descriptionHeight);
    }

    public function addExperience($start, $end, $position, $company, $description, $skills) {
        $top = $this->getY();
        $border = self::BORDER;

        // CIRCLE
        $this->Circle(self::CIRCLE_X, $top+self::CIRCLE_RADIUS, self::CIRCLE_RADIUS, 0, 360, 'F', null, self::BLACK_COLOR);

        // DATE FONT
        $this->SetFont(self::FONT_NAME, '', 12);

        // DATE START
        $this->SetX(self::DATE_X);
        $this->Cell(25, 0, $start, $border, 1, 'C');

        // DATE END
        $this->SetX(self::DATE_X);
        $this->Cell(25, 0, $end, $border, 1, 'C');

        // DESCRIPTION
        $this->SetXY(self::DESCRIPTION_X, $top);
        $this->SetFont(self::FONT_NAME, 'B', 12);
        $this->Multicell(65, 0, $position, $border, 'L', 0, 0);
        $this->SetFont(self::FONT_NAME, '', 12);
        $this->Multicell(80, 0, $company, $border, 'R');

        $this->SetX(self::DESCRIPTION_X);
        $this->SetFont(self::FONT_NAME, '', 12);
        $this->Multicell(145, 0, $description, $border, 'L');

        $this->SetX(self::DESCRIPTION_X);
        $this->SetFont(self::FONT_NAME_LIGHT, 'I', 12);
        $this->Multicell(145, 0, $skills, $border, 'L');

        $this->addY(self::PARAGRAPH_MARGIN_BOTTOM);

        return $this->getY() - $top;
    }

    public function addExperienceLine($y1, $height) {
        $x = self::CIRCLE_X;
        
        $this->SetAlpha(0.9);
        $this->Line($x, $y1+self::CIRCLE_RADIUS, $x, $y1+$height+self::CIRCLE_RADIUS, ['width' => self::LINE_WIDTH, 'color' => self::BLACK_COLOR]);
        $this->SetAlpha(1);
    }

    public function getFreePageHeight() {
        $y = $this->getY();
        $pageheight = $this->getPageHeight();
        $marginBottom = self::MARGIN_BOTTOM;

        return $pageheight - $marginBottom - $y;
    }

    public function getTrainingHeight($start, $end, $name, $school, $location, $skills) {
        $top = $this->getY();
        $border = self::BORDER;
        $width = self::TRAINING_WIDTH;

        $height = 0;

        // HEADER
        $this->SetFont(self::FONT_NAME, 'B', 12);
        $height += $this->GetStringHeight($width, "$start-$end $school");

        // NAME
        $this->SetFont(self::FONT_NAME, '', 12);
        $height += $this->GetStringHeight($width, "$name, $location");

        $this->SetFont(self::FONT_NAME_LIGHT, 'I', 12);
        $height += $this->GetStringHeight($width, $skills);

        $height += self::PARAGRAPH_MARGIN_BOTTOM;

        return $height;
    }

    public function addTraining($start, $end, $name, $school, $location, $skills) {
        $top = $this->getY();
        $border = self::BORDER;
        $width = self::TRAINING_WIDTH;

        // HEADER
        $this->SetFont(self::FONT_NAME, 'B', 12);
        $this->Multicell($width, 0, "$start-$end $school", $border, 'L');

        // NAME
        $this->SetFont(self::FONT_NAME, '', 12);
        $this->Multicell($width, 0, "$name, $location", $border, 'L');

        $this->SetFont(self::FONT_NAME_LIGHT, 'I', 12);
        $this->Multicell($width, 0, $skills, $border, 'L');

        $this->addY(self::PARAGRAPH_MARGIN_BOTTOM);

        return $this->getY() - $top;
    }

    public function getSkillGroupheight($name) {
        $this->SetFont(self::FONT_NAME, 'B', 12);
        return $this->getStringHeight(self::SKILL_WIDTH, $name);
    }

    public function addSkillGroup($name) {
        $border = self::BORDER;
        $this->SetFont(self::FONT_NAME, 'B', 12);
        $this->Multicell(self::SKILL_WIDTH, 0, $name, $border, 'L');
    }

    public function getSkillHeight($name, $level) {
        $circleR = self::SKILL_CIRCLE_RADIUS;
        $width = self::SKILL_WIDTH;
        $lineWidth = self::SKILL_CIRCLE_LINE_WIDTH;

        $circleHeight = $circleR * 2 + $lineWidth * 2;

        $this->SetFont(self::FONT_NAME, '', 12);
        $nameHeight = $this->getStringHeight($width - 2 * $circleR, $name);

        return max([$nameHeight, $circleHeight]);
    }

    public function addSkill($name, $level) {
        $border = self::BORDER;
        
        $y = $this->getY();
        $x = $this->getX();
        $circleR = self::SKILL_CIRCLE_RADIUS;
        $lineWidth = self::SKILL_CIRCLE_LINE_WIDTH;
        $width = self::SKILL_WIDTH;
        $circleX = $x+$width-$circleR;
        $circleY = $y+$circleR+$lineWidth;
        
        $startAng = 0;
        $endAng = ($level / 6) * 360;
        $rotation = 360 - $endAng + 90;

        $this->SetFont(self::FONT_NAME, '', 12);
        $this->Multicell($width - 2 * $circleR, 0, $name, $border, 'L');
        
        // background circle skills
        $this->SetLineStyle(array('width' => $lineWidth, 'color' => self::BLACK_COLOR));
        $this->SetAlpha(0.1);
        $this->Circle($circleX, $circleY, $circleR, 0, 360, 'D', null, self::BLUE_COLOR);
        $this->SetAlpha(1);

        // circle skills
        $this->StartTransform();
        $this->Rotate($rotation, $circleX, $circleY);
        $this->SetLineStyle(array('width' => $lineWidth, 'color' => self::BLACK_COLOR));
        $this->Circle($circleX, $circleY, $circleR, $startAng, $endAng, 'D', null, self::BLUE_COLOR);
        $this->StopTransform();
    }

    public function Footer() {
        $this->Rect(0, 297-15, $this->getPageWidth(), 15, 'F', null, self::BLUE_COLOR);
        
        $this->Cell(0, 0, 'samuelbucher.ovh', 0, 1, 'L', false, 'https://samuelbucher.ovh');
    }
}
