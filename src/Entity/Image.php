<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
	 *
	 * @Assert\File(mimeTypes={ "image/jpeg", "image/png" })
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255)
	 * @Gedmo\Translatable
     */
    private $alternative;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\File(mimeTypes={ "video/mp4" })
     */
    private $video;
    

    public function getId()
    {
        return $this->id;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file): self
    {
		$this->file = $file;

        return $this;
    }

    public function getAlternative()
    {
        return $this->alternative;
    }

    public function setAlternative(string $alternative)
    {
        $this->alternative = $alternative;

        return $this;
    }

    public function getProject()
    {
        return $this->project;
    }

    public function setProject(?Project $project)
    {
        $this->project = $project;

        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setVideo($video): self
    {
        $this->video = $video;

        return $this;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeFile() {
        if ($this->getFile() && file_exists($this->getFile())) {
            unlink($this->getFile());
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeVideo() {
        if ($this->getVideo() && file_exists($this->getVideo())) {
            unlink($this->getVideo());
        }
    }

}
