<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Link;
use App\Entity\Project;
use App\Form\ImageType;
use App\Form\LinkType;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/project", name="admin_")
 */
class AdminProjectController extends AbstractController
{
    /**
     * @Route("/", name="project_index", methods="GET")
     */
    public function index(ProjectRepository $projectRepository): Response
    {
        return $this->render('admin_project/index.html.twig', ['projects' => $projectRepository->findAll()]);
    }

    /**
     * @Route("/new", name="project_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
		return $this->generateForm($request, new Project());
    }

    /**
     * @Route("/{id}/edit", name="project_edit", methods="GET|POST")
     */
    public function edit(Request $request, Project $project): Response
    {
		return $this->generateForm($request, $project);
    }
    
    private function generateForm($request, $project) {
		$form = $this->createForm(ProjectType::class, $project);
		$form->handleRequest($request);
	
		if ($form->isSubmitted()) {
			
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($project);
				$em->flush();
				
				$this->addFlash('notice', 'Saved');
				
				return $this->redirectToRoute('admin_project_index');
			} else {
				foreach ($form->getErrors() as $error) {
					$this->addFlash('error', $error->getMessage());
				}
			}
		}
	
		return $this->render('admin_project/edit.html.twig', [
			'project' => $project,
			'form' => $form->createView(),
		]);
	}

    /**
     * @Route("/{id}/delete", name="project_delete", methods="GET")
     */
    public function delete(Request $request, Project $project): Response
    {
		$em = $this->getDoctrine()->getManager();
		$em->remove($project);
		$em->flush();
	
		$this->addFlash('notice', 'Deleted');

        return $this->redirectToRoute('admin_project_index');
	}
	
	/**
	 * @Route("/{id}/links", name="project_links", methods="GET")
	 */
	public function links(Request $request, Project $project): Response
	{
		return $this->render('admin_project/links.html.twig', ['project' => $project]);
	}
	
	/**
	 * @Route("/{id}/links/new", name="project_links_new", methods="GET|POST")
	 */
	public function linksNew(Request $request, Project $project): Response
	{
		$link = new Link();
		$link->setProject($project);
		
		return $this->generateLinkForm($request, $project, $link);
	}
	
	/**
	 * @Route("/{project}/links/{link}/edit", name="project_links_edit", methods="GET|POST")
	 */
	public function linksEdit(Request $request, Project $project, Link $link): Response
	{
		return $this->generateLinkForm($request, $project, $link);
	}
	
	private function generateLinkForm(Request $request, Project $project, Link $link) {
		
		$oldFile = $link->getIcon();
		
		$form = $this->createForm(LinkType::class, $link);
		$form->handleRequest($request);
		
		if ($form->isSubmitted()) {
			
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				
				$fileUploader = new FileUploader();
				$file = $link->getIcon();
				$fileUpdated = false;
				
				if ($file instanceof UploadedFile) {
					$fileName = $fileUploader->uploadFile($file);
					$link->setIcon($fileName);
					$fileUpdated = true;
				} else if ($oldFile) {
					$link->setIcon($oldFile);
				}
				
				$em->persist($link);
				$em->flush();
				
				if ($fileUpdated) {
					$fileUploader->removeFile($oldFile);
				}
				
				$this->addFlash('notice', 'Saved');
				
				return $this->redirectToRoute('admin_project_links', ['id' => $project->getId()]);
			} else {
				foreach ($form->getErrors() as $error) {
					$this->addFlash('error', $error->getMessage());
				}
			}
		}
		
		return $this->render('admin_project/edit_link.html.twig', [
			'link' => $link,
			'project' => $project,
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/{project}/links/{link}/delete", name="project_links_delete", methods="GET")
	 */
	public function deleteLink(Request $request, Project $project, Link $link): Response
	{
		$em = $this->getDoctrine()->getManager();
		$em->remove($link);
		$em->flush();
		
		$this->addFlash('notice', 'Deleted');
		
		return $this->redirectToRoute('admin_project_links', ['id' => $project->getId()]);
	}
    
	/**
	 * @Route("/{id}/images", name="project_images", methods="GET")
	 */
	public function images(Request $request, Project $project): Response
	{
		return $this->render('admin_project/images.html.twig', ['project' => $project]);
	}
	
	/**
	 * @Route("/{id}/images/new", name="project_images_new", methods="GET|POST")
	 */
	public function imagesNew(Request $request, Project $project): Response
	{
		$image = new Image();
		$image->setProject($project);
		
		return $this->generateImageForm($request, $project, $image);
	}
	
	/**
	 * @Route("/{project}/images/{image}/edit", name="project_images_edit", methods="GET|POST")
	 */
	public function imagesEdit(Request $request, Project $project, Image $image): Response
	{
		return $this->generateImageForm($request, $project, $image);
	}
	
	private function generateImageForm(Request $request, Project $project, Image $image) {
		
		$oldFile = $image->getFile();
        $oldvideo = $image->getVideo();
		
		$form = $this->createForm(ImageType::class, $image);
		$form->handleRequest($request);
		
		if ($form->isSubmitted()) {
			
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				
				$fileUploader = new FileUploader();
				$file = $image->getFile();
				$fileUpdated = false;
				
				if ($file instanceof UploadedFile) {
					$fileName = $fileUploader->uploadFile($file);
					$image->setFile($fileName);
					$fileUpdated = true;
				} else if ($oldFile) {
					$image->setFile($oldFile);
				}


                $video = $image->getVideo();
                $videoUploaded = false;
                if ($video instanceof UploadedFile) {
                    $fileName = $fileUploader->uploadFile($video);
                    $image->setVideo($video);
                    $videoUploaded = true;
                } else if ($oldFile) {
                    $image->setVideo($oldvideo);
                }
				
				$em->persist($image);
				$em->flush();
				
				if ($fileUpdated) {
					$fileUploader->removeFile($oldFile);
				}

				if ($videoUploaded) {
                    $fileUploader->removeFile($oldvideo);
                }
				
				$this->addFlash('notice', 'Saved');
				
				return $this->redirectToRoute('admin_project_images', ['id' => $project->getId()]);
			} else {
				foreach ($form->getErrors() as $error) {
					$this->addFlash('error', $error->getMessage());
				}
			}
		}
		
		return $this->render('admin_project/edit_image.html.twig', [
			'image' => $image,
			'project' => $project,
			'form' => $form->createView(),
		]);
	}
	
	/**
	 * @Route("/{project}/images/{image}/delete", name="project_image_delete", methods="GET")
	 */
	public function deleteImage(Request $request, Project $project, Image $image): Response
	{
		$em = $this->getDoctrine()->getManager();
		$em->remove($image);
		$em->flush();
		
		$this->addFlash('notice', 'Deleted');
		
		return $this->redirectToRoute('admin_project_images', ['id' => $project->getId()]);
	}
}
