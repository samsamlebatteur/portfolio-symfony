<?php

namespace App\Controller;

use App\Component\AbstractRestController;
use App\Entity\Training;
use App\Form\TrainingType;
use App\Repository\TrainingRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/admin/training", name="admin_training_")
 */
class AdminTrainingController extends AbstractRestController
{
	function __construct(TranslatorInterface $translator, TrainingRepository $trainingRepository) {
		parent::__construct($translator, $trainingRepository, TrainingType::class, Training::class, "admin_training_", $translator->trans('skillgroup'));
	}
}
