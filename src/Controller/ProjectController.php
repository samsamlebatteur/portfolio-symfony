<?php

namespace App\Controller;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    /**
     * @Route("/project", name="project_index")
     */
    public function index(ProjectRepository $projectRepository)
    {
        return $this->render('project/index.html.twig', [
        	'projects' => $projectRepository->findBy(array(), array('date' => 'DESC'))
		]);
    }
	
	/**
	 * @Route("/project/{project}", name="project_show")
	 */
	public function show(Project $project)
	{
		return $this->render('project/show.html.twig', [
			'project' => $project
		]);
	}
}
