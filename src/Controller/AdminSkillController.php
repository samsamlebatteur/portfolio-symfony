<?php

namespace App\Controller;


use App\Component\AbstractRestController;
use App\Entity\Skill;
use App\Form\SkillType;
use App\Repository\SkillRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/admin/skill", name="admin_skill_")
 */
class AdminSkillController extends AbstractRestController
{
	function __construct(TranslatorInterface $translator, SkillRepository $skillRepository) {
		parent::__construct($translator, $skillRepository, SkillType::class, Skill::class, "admin_skill_", $translator->trans('skill'));
	}
}
