<?php

namespace App\Controller;


use App\Entity\Experience;
use App\Form\ExperienceType;
use App\Repository\ExperienceRepository;
use App\Component\AbstractRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/admin/experience", name="admin_experience_")
 */
class AdminExperienceController extends AbstractRestController
{

	function __construct(TranslatorInterface $translator, ExperienceRepository $experienceRepository) {
		parent::__construct($translator, $experienceRepository, ExperienceType::class, Experience::class, "admin_experience_", $translator->trans('experience'));
	}

}
