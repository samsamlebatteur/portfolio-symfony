<?php

namespace App\Controller;


use App\Component\AbstractRestController;
use App\Entity\SkillGroup;
use App\Form\SkillGroupType;
use App\Repository\SkillGroupRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/admin/skill_group", name="admin_skill_group_")
 */
class AdminSkillGroupController extends AbstractRestController
{
	function __construct(TranslatorInterface $translator, SkillGroupRepository $skillGroupRepository) {
		parent::__construct($translator, $skillGroupRepository, SkillGroupType::class, SkillGroup::class, "admin_skill_group_", $translator->trans('skillgroup'));
	}
}
