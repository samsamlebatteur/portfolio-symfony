<?php

namespace App\Controller;

use App\Pdf\CV;
use App\Repository\ProjectRepository;
use App\Repository\TrainingRepository;
use App\Repository\ExperienceRepository;
use App\Repository\SkillGroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;


class HomeController extends AbstractController
{
	private $translator = null;
	
	public function __construct(TranslatorInterface $translator)
	{
		$this->translator = $translator;
	}
	
	/**
     * @Route("/", name="home")
     */
    public function index(
		ExperienceRepository $experienceRepository,
		TrainingRepository $trainingRepository,
		ProjectRepository $projectRepository,
		SkillGroupRepository $skillGroupRepository
		)
    {
        return $this->render('home/index.html.twig', [
			'projects' => $projectRepository->last(5),
			'trainings' => $trainingRepository->findBy(array(), array('start' => 'DESC')),
			'experiences' => $experienceRepository->findBy(array(), array('start' => 'DESC')),
			'skillGroups' => $skillGroupRepository->findBy(array(), array('displayOrder' => 'ASC', 'name' => 'ASC')),
		]);
	}
	
	/**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        return $this->render('home/contact.html.twig');
    }
	
	/**
	 * @Route("/resume", name="resume")
	 */
	public function resume(
		ExperienceRepository $experienceRepository, 
		TrainingRepository $trainingRepository,
		SkillGroupRepository $skillGroupRepository
		)
	{
		$experiences = $experienceRepository->findBy(array('cv' => 1), array('start' => 'DESC'));
		$trainings = $trainingRepository->findBy(array(), array('start' => 'DESC'));
		$skillGroups = $skillGroupRepository->findBy(array(), array('displayOrder' => 'ASC', 'name' => 'ASC'));
		
		$pdf = $this->generateCV($experiences, $trainings, $skillGroups);

		//return new Response();
		return new Response(
			$pdf->Output('cv.pdf', 'S'),
			200,
			array('Content-Type' => 'application/pdf')
		);
	}

	/**
	 * @Route("/resume/complete", name="complete_resume")
	 */
	public function completeResume(
		ExperienceRepository $experienceRepository, 
		TrainingRepository $trainingRepository,
		SkillGroupRepository $skillGroupRepository
		)
	{
		
		$experiences = $experienceRepository->findBy(array(), array('start' => 'DESC'));
		$trainings = $trainingRepository->findBy(array(), array('start' => 'DESC'));
		$skillGroups = $skillGroupRepository->findBy(array(), array('displayOrder' => 'ASC', 'name' => 'ASC'));
		
		$pdf = $this->generateCV($experiences, $trainings, $skillGroups);

		//return new Response();
		return new Response(
			$pdf->Output('example_001.pdf', 'S'),
			200,
			array('Content-Type' => 'application/pdf')
		);
	}

	private function formatExperienceDate($datetime) {
		$translator = $this->translator;
		$month = $translator->trans($this->formatMonth($datetime->format('m')));
		if (strlen($month) > 3) $month = mb_substr($month, 0, 3, 'UTF-8') . '.';
		$year = $datetime->format('Y');
		return "$month $year";
	}

	private function generateCV($experiences, $trainings, $skillGroups) {
		$translator = $this->translator;
		
		// create new PDF document
		$pdf = new CV();
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Samuel BUCHER');
		$pdf->SetTitle('Samuel BUCHER');
		$pdf->SetSubject('Curriculum Vitae');
		$pdf->SetKeywords('Samuel BUCHER, CV');

		$name = $translator->trans('Samuel BUCHER');
		$position = $translator->trans('Web developper, project manager');
		$email = $translator->trans('samuel.bucher@outlook.fr');
		$phone = $translator->trans('0672498943');
		$age = $translator->trans('xx years');
		$vehicule = $translator->trans('Driving License + car');
		$nationality = $translator->trans('French and Swiss nationality');
		$address = $translator->trans('personnal address');
		$zip = $translator->trans('personnal zip');
		$city = $translator->trans('personnal city');
		$country = $translator->trans('personnal country');

		$pdf->setHeaderInfo($name, $position, $email, $phone, $age, $vehicule, $nationality, $address, $zip, $city, $country);

        $pdf->AddPage();
        $pdf->SetTextColor(0,0,0);
        $pdf->addTitle($translator->trans('About me'));
        
        $pdf->addParagraph(190, $translator->trans('About me text'));

        $pdf->addTitle($translator->trans('Work experiences'));

		// calculate space between experiences
		$freePageHeight = $pdf->getFreePageHeight();
		$experiencesIndex = 0;

		foreach ($experiences as $experience) {
			$experienceHeight = $pdf->getExperienceHeight(
				$this->formatExperienceDate($experience->getStart()),
				$this->formatExperienceDate($experience->getEnd()),
				$experience->getPosition(),
				$experience->getCompany(),
				$experience->getDescription(),
				$experience->getSkills()
			);

			if ($freePageHeight < $experienceHeight) break;
			
			$freePageHeight -= $experienceHeight;
			$experiencesIndex++;
		}

		$experienceSpacing = 0;

		if ($experiencesIndex > 1) {
			$experienceSpacing = ceil($freePageHeight / ($experiencesIndex - 1));
		}

		// write experiences
		$experiencesLength = count($experiences);
		$experiencesIndex = 1;

		foreach ($experiences as $experience) {
			$pdf->setY($pdf->getY() + $experienceSpacing);

			$freePageHeight = $pdf->getFreePageHeight();
			$experienceHeight = $pdf->getExperienceHeight(
				$this->formatExperienceDate($experience->getStart()),
				$this->formatExperienceDate($experience->getEnd()),
				$experience->getPosition(),
				$experience->getCompany(),
				$experience->getDescription(),
				$experience->getSkills()
			);

			$freePageHeight = floor($freePageHeight);
			$experienceHeight = ceil($experienceHeight);

			if ($freePageHeight < $experienceHeight) {
				$pdf->SetLineStyle(['dash' => 4]);
				$pdf->addExperienceLine($pdf->getY(), $freePageHeight);
				$pdf->AddPage();
			}

			$lastY = $pdf->getY();

			$height = $pdf->addExperience(
				$this->formatExperienceDate($experience->getStart()),
				$this->formatExperienceDate($experience->getEnd()),
				$experience->getPosition(),
				$experience->getCompany(),
				$experience->getDescription(),
				$experience->getSkills()
			);

			if ($experiencesIndex !== $experiencesLength) {
				$pdf->SetLineStyle(['dash' => 0]);
				$pdf->addExperienceLine($lastY, $height + $experienceSpacing);
			}

			$experiencesIndex++;
		}

		// add spacing after last experience
		$pdf->setY($pdf->getY() + $experienceSpacing);

		// calculate training height
		$trainingHeight = 0;
		$freePageHeight = $pdf->getFreePageHeight();

		foreach ($trainings as $training) {
			$startDate = $training->getStart();
			$endDate = $training->getEnd();

			$startYear = $startDate->format('Y');
			$endYear = $endDate->format('Y');
			$trainingHeight += $pdf->getTrainingHeight(
				"$startYear",
				"$endYear",
				$training->getName(),
				$training->getSchool(),
				$training->getLocation(),
				$training->getSkills()
			);
		}

		// calculate skills height
		$skillGroupsCount = count($skillGroups);
		$skillGroupColumnIndex = 0;
		$skillGroupIndex = 0;
		$skillGroupHeights = [];
		$skillGroupHeight = 0;

		foreach ($skillGroups as $skillGroup) {
			if ($skillGroupIndex == ceil($skillGroupsCount / 2)) {
				array_push($skillGroupHeights, $skillGroupHeight);
				$skillGroupHeight = 0;
				$skillGroupColumnIndex++;
			}

			$skillGroupHeight += $pdf->getSkillGroupHeight($skillGroup->getName());

			foreach ($skillGroup->getSkills() as $skill) {
				$skillGroupHeight += $pdf->getSkillHeight($skill->getName(), $skill->getLevel());
			}

			$skillGroupHeight += CV::PARAGRAPH_MARGIN_BOTTOM;
			$skillGroupIndex++;
		}

		array_push($skillGroupHeights, $skillGroupHeight);

		$freePageHeight = floor($freePageHeight);
		$trainingHeight = ceil($trainingHeight);
		$maxSkillGroupHeight = ceil(max($skillGroupHeights));

		if ($freePageHeight <= $trainingHeight
			|| $freePageHeight <= $maxSkillGroupHeight) {
			$pdf->addPage();
		}

		// INSERT TRAINING
		$trainingY = $pdf->getY();

		$pdf->addTitle($translator->trans('Training'));
		
		foreach ($trainings as $training) {
			$startDate = $training->getStart();
			$endDate = $training->getEnd();

			$startYear = $startDate->format('Y');
			$endYear = $endDate->format('Y');
			$pdf->addTraining(
				"$startYear",
				"$endYear",
				$training->getName(),
				$training->getSchool(),
				$training->getLocation(),
				$training->getSkills()
			);
		}

		$skillsX = CV::TRAINING_WIDTH + 15;
		$skillsWidth = CV::SKILL_WIDTH + 5;

		$pdf->SetY($trainingY);
		$pdf->SetX($skillsX);
		$pdf->addTitle($translator->trans('Skills'));
		
		// count
		$skillGroupsCount = count($skillGroups);
		$skillGroupIndex = 0;
		$skillGroupColumnIndex = 0;

		// last Y
		$lastSkillGroupY = $pdf->GetY();
		
		foreach ($skillGroups as $skillGroup) {
			if ($skillGroupIndex == ceil($skillGroupsCount / 2)) {
				$skillGroupColumnIndex++;
				$pdf->SetY($lastSkillGroupY);
			}
			
			$pdf->SetX($skillsX + $skillGroupColumnIndex * $skillsWidth);
			$skillGroupName = $skillGroup->getName();
			$pdf->addSkillGroup($skillGroupName);

			foreach ($skillGroup->getSkills() as $skill) {
				$pdf->SetX($skillsX + $skillGroupColumnIndex * $skillsWidth);
				$pdf->addSkill($skill->getName(), $skill->getLevel());
			}

			$pdf->addY(CV::PARAGRAPH_MARGIN_BOTTOM);

			$skillGroupIndex++;
		}

		return $pdf;
	}

	public function formatMonth($month) {
		switch($month) {
			case 1:
				return 'January';
			case 2:
				return 'February';
			case 3:
				return 'March';
			case 4:
				return 'April';
			case 5:
				return 'May';
			case 6:
				return 'June';
			case 7:
				return 'July';
			case 8:
				return 'August';
			case 9:
				return 'September';
			case 10:
				return 'October';
			case 11:
				return 'November';
			case 12:
				return 'December';
			default:
				return '';
		}
	}
	
	/**
	 * @Route("/sitemap.{_format}", name="sitemap", Requirements={"_format" = "xml"})
	 */
	public function sitemap(ProjectRepository $projectRepository)
	{
		return $this->render('home/sitemap.xml.twig', [
			'projects' => $projectRepository->findBy(array()),
		]);
	}
}
