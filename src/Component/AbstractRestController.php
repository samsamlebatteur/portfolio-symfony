<?php

namespace App\Component;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractRestController extends AbstractController {

    function __construct($translator, $repository, $form, $entity, $route, $title) {
        $this->translator = $translator;
        $this->repository = $repository;
        $this->form = $form;
        $this->entity = $entity;
        $this->route = $route;
        $this->title = $title;
    }

    /**
     * @Route("/", name="index", methods="GET")
     */
    public function index(): Response
    {
        return $this->render('abstract/index.html.twig', [
            'items' => $this->repository->findAll(),
            'route' => $this->route,
            'title' => $this->title,
            ]);
    }

    /**
     * @Route("/new", name="new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
		return $this->generateForm($request, new $this->entity());
    }

    /**
     * @Route("/{id}/edit", name="edit", methods="GET|POST")
     */
    public function edit(Request $request, $id): Response
    {
        $item = $this->repository->findOneBy(['id' => $id]);

        if (!$item) {
            $this->addFlash('error', $this->translator->trans('rest.notfound'));
            return $this->redirectToRoute("{$this->route}index");
        }

		return $this->generateForm($request, $item);
    }
    
    private function generateForm($request, $item) {
		$form = $this->createForm($this->form, $item);
		$form->handleRequest($request);
	
		if ($form->isSubmitted()) {
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($item);
				$em->flush();
				
				$this->addFlash('notice', $this->translator->trans('rest.saved'));
				
				return $this->redirectToRoute("{$this->route}index");
			} else {
				foreach ($form->getErrors() as $error) {
					$this->addFlash('error', $error->getMessage());
				}
			}
		}
	
		return $this->render('abstract/edit.html.twig', [
			'item' => $item,
            'form' => $form->createView(),
            'route' => $this->route,
            'title' => $this->title,
		]);
	}

    /**
     * @Route("/{id}/delete", name="delete", methods="GET")
     */
    public function delete(Request $request, $id): Response
    {
        $item = $this->repository->findOneBy(['id' => $id]);

        if (!$item) {
            $this->addFlash('error', $this->translator->trans('rest.notfound'));
            return $this->redirectToRoute("{$this->route}index");
        }

		$em = $this->getDoctrine()->getManager();
		$em->remove($item);
		$em->flush();
	
        $this->addFlash('notice', $this->translator->trans('rest.deleted'));
        
        return $this->redirectToRoute("{$this->route}index");
	}
}