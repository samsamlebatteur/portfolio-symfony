<?php

namespace App\Component;

use Gedmo\Translatable\Translatable;

abstract class AbstractEntity implements Translatable
{
    abstract function getTitle();

    abstract function getSubTitle();
}
