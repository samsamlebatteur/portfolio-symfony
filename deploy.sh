#!/bin/bash

echo "rebuild docker image";
docker-compose build;

echo "up docker image";
docker-compose up -d;