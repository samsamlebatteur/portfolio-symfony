-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: portfolio
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ext_log_entries`
--

DROP TABLE IF EXISTS `ext_log_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ext_log_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `object_id` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_class_lookup_idx` (`object_class`),
  KEY `log_date_lookup_idx` (`logged_at`),
  KEY `log_user_lookup_idx` (`username`),
  KEY `log_version_lookup_idx` (`object_id`,`object_class`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ext_log_entries`
--

LOCK TABLES `ext_log_entries` WRITE;
/*!40000 ALTER TABLE `ext_log_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `ext_log_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ext_translations`
--

DROP TABLE IF EXISTS `ext_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ext_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`),
  KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ext_translations`
--

LOCK TABLES `ext_translations` WRITE;
/*!40000 ALTER TABLE `ext_translations` DISABLE KEYS */;
INSERT INTO `ext_translations` VALUES (1,'en','App\\Entity\\Project','title','1','Proximity'),(2,'en','App\\Entity\\Project','description','1','Proximity is a cross-channel publishing platform to accelerate the enterprise communication.\r\n\r\nStores customise marketing operation medias. Medias can be printed or digital (Posters, SMS, MMS, Voice, Email ...).\r\n\r\nProximity an application developed by Elpev in Wittelsheim, France.'),(3,'en','App\\Entity\\Project','subtitle','1','Web to print application'),(4,'en','App\\Entity\\Project','skills','1','Analysis, Design, Integration, Web Development, Maintenance.\r\n\r\nSymfony2, PHP5, AngularJS, HTML5, CSS3, Bulma, ELM, Javascript, SQL, Go.'),(5,'en','App\\Entity\\Image','alternative','1','Teasing'),(6,'en','App\\Entity\\Link','name','1','Elpev'),(7,'en','App\\Entity\\Project','title','3','Love insanity'),(8,'en','App\\Entity\\Project','description','3','We will make you love madness!\r\n\r\nAnswer the questions and we will find your fit !\r\n\r\nThe application calculates affinities and forms teams. It used to organize Christmas party 2018.'),(9,'en','App\\Entity\\Project','subtitle','3','Affinity creator'),(10,'en','App\\Entity\\Project','skills','3','Analysis, Design, Design, Integration, Web Development.\r\n\r\nSymfony 4, PHP7, Angular 2, HTML5, CSS3.'),(12,'en','App\\Entity\\Image','alternative','4','Home page'),(13,'en','App\\Entity\\Project','title','2','UAnimations'),(14,'en','App\\Entity\\Project','description','2','The points of sale build their own communication based on national and specific campaigns: remodeling, birthday, new services ...\r\n\r\nUanimations an application promoted by Elpev in Wittelsheim, France.'),(15,'en','App\\Entity\\Project','subtitle','2','A web platform for points of sale animation'),(16,'en','App\\Entity\\Project','skills','2','Analysis, Design, Integration, Web Development.\r\n\r\nAngularJS, HTML5, CSS3, Javascript, SQL, Go.'),(17,'en','App\\Entity\\Image','alternative','6','Home page'),(18,'en','App\\Entity\\Link','name','3','Go on Love insanity'),(19,'en','App\\Entity\\Project','title','4','Localized notifications'),(20,'en','App\\Entity\\Project','description','4','Beacons of a few centimeters can be placed anywhere (outdoors or indoors).\r\n\r\nThey constantly emit a unique identifier 10 meters around in Bluetooth LE 4.0 (Low Energy).\r\n\r\nOperation\r\nThe smartphone detects beacons nearby. Beacons detected list is sended to web service. If the detected beacon or beacons are known, the platform indicates an action to performed on mobile (show informations, download files, ...).\r\n\r\nPrototype made by stemys in Porrentruy, Switzerland'),(21,'en','App\\Entity\\Project','subtitle','4','Beacon management through web platform and mobile app'),(22,'en','App\\Entity\\Project','skills','4','Analysis, Design, Integration, Web Development.\r\n\r\nAngularJS, HTML5, CSS3, Javascript, J2EE (Spring).'),(23,'en','App\\Entity\\Image','alternative','7','Dashboard and mobile app'),(25,'en','App\\Entity\\Link','name','4','stemys'),(26,'en','App\\Entity\\Project','title','5','Iticity'),(27,'en','App\\Entity\\Project','description','5','Promote soft and physical activity, using of urban space and proposing adapted routes to user profile and interests.\r\n\r\nUsers chooses a city to discover. Next they customizes trip it by adding or removing interrest points.\r\n\r\nApplication built by Stemys for the O2 Foundation in Porrentruy, Switzerland'),(28,'en','App\\Entity\\Project','subtitle','5','Soft trip web app'),(29,'en','App\\Entity\\Project','skills','5','Analysis, Design, Integration, Web Development.\r\n\r\nAngularJS, HTML5, CSS3, JavaScript, Google Maps API, Materialize, J2EE (Spring).'),(31,'en','App\\Entity\\Image','alternative','10','Explore the surrounding area'),(32,'en','App\\Entity\\Image','alternative','11','Edit your trip'),(33,'en','App\\Entity\\Project','title','6','Caborde'),(34,'en','App\\Entity\\Project','description','6','Caborde is a viti-cultural area, located in Orbagna. Exchange place and important tourist meeting point in the South Revermont region.\r\n\r\nThey wants to promote differents aspects of the South Revermont territory through new technologies.\r\n\r\nThe partnership with Caborde resulted in the creation of a mobile application, which based on a real-time positioning, displays a 360-degree panoramic view enriched with updated cartographic and event data !\r\n\r\nStudent project, teamwork.'),(35,'en','App\\Entity\\Project','subtitle','6','Augmented reality application'),(36,'en','App\\Entity\\Project','skills','6','Communication, Graphics, Ergonomics (Mobile First), Design, Programming.\r\n\r\nCakePHP, HTML5, CSS3, SQL, Swift (XCode).'),(38,'en','App\\Entity\\Image','alternative','13','Augmented reality application'),(39,'en','App\\Entity\\Project','title','7','Montbéli\'art'),(40,'en','App\\Entity\\Project','description','7','Montbéli\'art is a student collective, artistic and associative, which aims to promote the heritage of Montbéliard.\r\n\r\nMontbéliard museum moves its collection, which was hidden in the reserve until now. All works are now exposed during an atypical exhibition \"Le musée mis à nu\" from April 11, 2014 to August 17, 2014. But when the move was over, a work is missing ...\r\n\r\nThrough a transmedia scenario using various media (anamorphosis,  on-board camera recording, cameras surveillance, sales announcement ...), the public was able to search for the lost painting.\r\n\r\nThe communication established (press articles, clean tags, posters, etc.) encouraged the public target to participate. The best participants received various prizes, funded by our partners (Museum Pass, movie tickets, gift cards, Citédo tickets ...).\r\n\r\nStudent project, teamwork.'),(41,'en','App\\Entity\\Project','subtitle','7','Student collective promoting Montbéliard heritage'),(42,'en','App\\Entity\\Project','skills','7','Communication, Scriptwriting, Writing, Graphic Design, Web Development, Transmedia.\r\nWordpress.'),(43,'en','App\\Entity\\Image','alternative','14','Poster'),(44,'en','App\\Entity\\Project','title','9','Banana launch'),(45,'en','App\\Entity\\Project','description','9','Production of a 3D animation. The scene features minions, references \"Despicable Me\" cartoon.\r\n\r\nAnimation designed in team within my master degree studies, using 3DS Max software.'),(46,'en','App\\Entity\\Project','subtitle','9','Minion cartoon'),(47,'en','App\\Entity\\Project','skills','9','Storyboarding, modeling, mapping.'),(48,'en','App\\Entity\\Project','title','10','Propositional formulas calculation'),(49,'en','App\\Entity\\Project','description','10','The application is able to apply various operations to user\'s propositional formulas (conversion to normal conjunctive form, conversion to disjunctive normal form, generation of truth table, LaTex code, etc.).\r\n\r\nUnzip and test the application, available in zip archive in the link below.'),(50,'en','App\\Entity\\Project','subtitle','10','Java application'),(51,'en','App\\Entity\\Project','skills','10','Analysis, Design, Development.\r\n\r\nJava (OOA).'),(53,'en','App\\Entity\\Image','alternative','16','Watch cartoon now'),(54,'en','App\\Entity\\Image','alternative','18','Application interface'),(55,'en','App\\Entity\\Link','name','6','stemys'),(56,'en','App\\Entity\\Link','name','7','La Caborde'),(57,'en','App\\Entity\\Link','name','8','Wordpress'),(58,'en','App\\Entity\\Link','name','9','Facebook'),(59,'en','App\\Entity\\Link','name','10','Youtube');
/*!40000 ALTER TABLE `ext_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alternative` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C53D045F166D1F9C` (`project_id`),
  CONSTRAINT `FK_C53D045F166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,1,'uploads/b00c7e52b1c383c943b2a3906b8c0caa.jpeg','Teasing',NULL),(4,3,'uploads/7ee34daad58ccbb898b901b89f61295a.jpeg','Page d\'accueil',NULL),(6,2,'uploads/870b92daccfb8199ecc94016b7d99d16.jpeg','Page d\'accueil',NULL),(7,4,'uploads/573837d158be480b211522fb8a1c7920.jpeg','Dashbord et application mobile',NULL),(10,5,'uploads/7d48521fa602cecd034af6be1d2daf81.jpeg','Explorez les environs',NULL),(11,5,'uploads/2e146eff1bef36e01929baf561c37c2b.jpeg','Construisez votre parcours',NULL),(13,6,'uploads/16a3af373ef2466ad032054dcff42562.jpeg','Application à réalité augmentée',NULL),(14,7,'uploads/cb67f30dfa508413496ac39b177537e6.jpeg','Affiche promotionelle',NULL),(16,9,'uploads/af03a8d4fc597fb9f0a4e4bf1808d68c.jpeg','Vidéo','uploads/421b47ffd946ca083b65cd668c6b17e6.mp4'),(18,10,'uploads/83e34b9dcc31a1276fa7ad65852b9087.jpeg','Interface de l\'application',NULL);
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_36AC99F1166D1F9C` (`project_id`),
  CONSTRAINT `FK_36AC99F1166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link`
--

LOCK TABLES `link` WRITE;
/*!40000 ALTER TABLE `link` DISABLE KEYS */;
INSERT INTO `link` VALUES (1,1,'ELPEV','uploads/d9fe8e1bcf8d4b7c157e140d67ba9abb.png','https://www.elpev.com'),(2,2,'ELPEV','uploads/134bc94f7f318dc05adf9810e5485a14.png','ELPEV'),(3,3,'Connectez vous sur Love Insanity','uploads/5ecc0da8340e1a43f3c4a926aebe64d4.png','https://loveinsanity.samuelbucher.ovh/login?authKey=abcdefghijklmnopqrstuvwxyz012345'),(4,4,'stemys','uploads/9e42dbac0b14766cd005d6dc882f0df5.png','http://www.stemys.io/'),(5,10,'Application','uploads/2e4f064618cfd3c61730760194ba828d.png','https://samuelbucher.ovh/uploads/Application_Java_Formules_Propositionnelles.zip'),(6,5,'stemys','uploads/03b7d3b01a3d5a6aa751d7de1de893cb.png','http://www.stemys.io/'),(7,6,'La Caborde','uploads/bde9f0da4e7b58030164bded7c9bf594.png','https://www.lacaborde-jura.fr/fr/'),(8,7,'Wordpress','uploads/65ef8b4f7f3638a1f277b51d6381b51f.png','http://montbeli.free.fr/'),(9,7,'Facebook','uploads/05673b4e9394bd4644101b7f00d0c76f.png','https://www.facebook.com/montbeliart'),(10,7,'Youtube','uploads/84e2d9462e25c784022c8bc9728156d9.png','https://www.youtube.com/user/montbeliart');
/*!40000 ALTER TABLE `link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skills` longtext COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'Proximity','Proximity est une plateforme de publication cross-canal, destinée à la communication des enseignes à réseau.\r\n\r\nLes magasins de l’enseigne peuvent ainsi personnaliser les supports de communications proposés dans le cadre d’une opération marketing. Ces supports peuvent être du print ou du digital (SMS, MMS, Voice, Email...).\r\n\r\nProximity une application développée par Elpev à Wittelsheim, France.','Un portail web to print dédié au point de vente','Analyse, Conception, Intégration, Développement Web, Maintenance.\r\n\r\nSymfony2, PHP5, AngularJS, HTML5, CSS3, Bulma, ELM, Javascript, SQL, Go.','2015-07-30','2019-03-28 21:19:27'),(2,'UAnimations','Les magasins accèdent à des opérations commerciales personnalisées déclinées de la communication nationale, et des opérations spécifiques qui rythment la vie du point de vente : remodelage, anniversaire, ouverture de nouveaux services ...\r\n\r\nUanimations une application propulsée par Elpev à Wittelsheim, France.','Une plateforme web d\'animation locale des points de vente','Analyse, Conception, Intégration, Développement Web.\r\n\r\nAngularJS, HTML5, CSS3, Javascript, SQL, Go.','2016-07-10','2019-03-28 21:19:27'),(3,'Love Insanity','Nous allons vous faire aimer la folie ! \r\n\r\nRépondez aux questions et nous vous trouverons chaussure à votre pied !\r\n\r\nL\'application calcule les affinités entre utilisateurs et forme des groupes.\r\nElle a été utilisée dans l\'organisation d\'une fête de Noël.','Créateur d’affinité sur mesure','Analyse, Conception, Design, Intégration, Développement Web.\r\n\r\nSymfony 4, PHP7, Angular 2, HTML5, CSS3.','2018-01-10','2019-03-28 21:19:27'),(4,'Notifications contextualisées','Beacon signifie balise. Ces boîtiers de quelques centimètres peuvent être placés n\'importe où, en extérieur ou en intérieur.\r\n\r\nIls émettent leur identifiant dans un rayon pouvant aller jusqu\'à quelques dizaines de mètres en Bluetooth LE 4.0 (Low Energy) suivant un intervalle de temps très court.\r\n\r\nFonctionnement\r\nLe smartphone détecte des beacons à proximité. L’application mobile notifie à la plateforme web que des beacons sont à différentes proximités. Si le ou les beacons détectés sont connus, la plateforme indique l’action à réaliser sur le mobile (push d’information, téléchargement d’une brochure, …).\r\n\r\nPrototype réalisé par Stemys à Porrentruy, Suisse','Plateforme web de gestion de beacons et application mobile','Analyse, Conception, Intégration, Développement web.\r\n\r\nAngularJS, HTML5, CSS3, Javascript, J2EE (Spring).','2015-06-09','2019-03-31 16:50:14'),(5,'Iticity','Promouvoir la mobilité douce et la pratique d\'une activité physique à travers l\'utilisation de l\'espace urbain par des itinéraires adaptés au profil de l\'utilisateur et de ses intérêts.\r\n\r\nLes utilisateurs pourront rechercher un parcours selon leurs envies dans la ville de leur choix. Ils peuvent ensuite le personnaliser par l\'ajout ou la suppression de points intermédiaires. Le parcours sélectionné pourra être exporté en PDF.\r\n\r\nApplication construite par Stemys pour la Fondation O2, Porrentruy, Suisse','Plateforme web de génération d\'itinéraires','Analyse, Conception, Intégration, Développement web.\r\n\r\nAngularJS, HTML5, CSS3, Javascript, Google Maps API, Materialize, J2EE (Spring).','2015-06-13','2019-03-28 21:19:27'),(6,'Caborde','La Caborde, aire viti-culturelle, localisée à Orbagna, est un lieu d\'échange et un marqueur touristique important dans la région du Sud Revermont.\r\n\r\nLa Caborde souhaite promouvoir les différents aspects du térritoire du Sud Revermont par le biais des nouvelles technologies.\r\n\r\nLe partenariat avec la Caborde a abouti sur la réalisation d\'une application mobile, qui sur la base d\'un positionnement temps réel, affiche une vision panoramique à 360 degrés enrichie de données cartographiques et événementielles mises à jours!\r\n\r\nProjet étudiant, réalisé en équipe.','Application mobile à réalité augmentée','Communication, Graphisme, Ergonomie (Mobile First), Conception, Programmation.\r\n\r\nCakePHP, HTML5, CSS3, SQL, Swift (XCode).','2015-05-07','2019-03-28 21:19:27'),(7,'Montbéli\'art','Montbéli’art est un collectif étudiant, artistique et associatif, qui a pour objectif la valorisation du patrimoine de Montbéliard.\r\n\r\nLe Château de Montbéliard vient de déménager sa collection Beaux-arts, stockée jusqu\'alors à l\'abri des regards dans la réserve. Toutes ces œuvres sont désormais présentées exceptionnellement au public au cours d\'une exposition atypique \"Le musée mis à nu\" du 11 Avril 2014 au 17 Août 2014. Mais une fois le déménagement terminé, une oeuvre manque à l\'appel...\r\n\r\nPar le bais d\'un scénario transmédia utilisant des supports variés (anamorphoses, enregistrement d\'une caméra embarquée, extraits de caméras de surveillance, annonce de vente sur Leboncoin...), le public a pu se mettre à la recherche du tableau dans Montbéliard.\r\n\r\nLa communication mise en place (articles de presse, clean tags, affiche...) ont permis d\'inciter le public visé à participer. Les meilleurs participants ont pu recevoir diverses lots, financés par nos partenaires (Pass Museum, places de cinéma, cartes cadeaux, entrées Citédo...).\r\n\r\nProjet étudiant, réalisé en équipe.','Projet étudiant, destiné à la valorisation du patrimoine de Montbéliard','Communication, Scénarisation, Écriture, Graphisme, Développement Web, Transmédia.\r\nWordpress.','2014-11-07','2019-03-28 21:19:27'),(9,'Banana launch','Réalisation d\'une animation 3D dans le cadre du Master Produits et Services Multimédias.\r\n\r\nMise en scène de minions, en référence au film \"Moi, Moche et Méchant\".\r\n\r\nAnimation 3D modélisée à l\'aide du logiciel 3DS Max.','Animation Minions en 3D','Storybording, Modèlisation, Mapping.','2014-02-07','2019-03-28 21:19:27'),(10,'Calcul de formules propositionnelles','L\'application est capable d\'appliquer diverses opérations aux formules propositionnelles de l\'utilisateur (conversion en forme conjonctive normale, conversion en forme normale disjonctive, génération de table de vérité, code LaTex, etc.).\r\n\r\nDécompressez et testez l\'application, disponible en archive zip dans le lien ci-dessous.','Application Java','Analyse, Conception, Développement.\r\n\r\nJava (POO).','2014-01-10','2019-03-28 21:19:27');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'samuel','samuel','samuel.bucher@outlook.fr','samuel.bucher@outlook.fr',1,NULL,'$2y$13$nmbZJDUEl9aRlu7OBL2aAexM.ydHjtBWb0rdXnFaP.7ZZ5q9kLob.','2019-03-31 16:49:49',NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),(3,'sbucher','sbucher','s.bucher@elpev.com','s.bucher@elpev.com',1,NULL,'$2y$13$lj/zYY7YCOsmcBFYa3STcuB4l/RiOQBUjgEKMGh0KHr/VUibA.MZu','2019-03-28 12:24:07',NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-04 21:15:11
